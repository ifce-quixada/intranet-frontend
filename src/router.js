import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import SignIn from './views/SignIn.vue'
import SignOut from './views/SignOut.vue'
import Dashboard from './views/Dashboard.vue'

// Internal modules
import IndexBranches from './views/Branches/Index.vue'
import IndexInformatives from './views/Informatives/Index.vue'
import IndexFrequencies from './views/Frequencies/Index.vue'
import IndexTickets from './views/Tickets/Index.vue'
import CreateTicketsTI from './views/Tickets/CreateTI.vue'
import CreateTicketsInfra from './views/Tickets/CreateInfra.vue'

import IndexAgenda from './views/Agenda/Index.vue'
import CreateAgendaDentist from './views/Agenda/CreateDentist.vue'

import IndexStudents from './views/Students/Index.vue'
import CheckAcademicEmail from './views/Students/CheckAcademicEmail.vue'
import RequestDegree from './views/Students/RequestDegree.vue'
import InternshipDocs from './views/Students/InternshipDocs.vue'
import BeginInternshipDocs from './views/Students/BeginInternshipDocs.vue'
import DuringInternshipDocs from './views/Students/DuringInternshipDocs.vue'
import EndingInternshipDocs from './views/Students/EndingInternshipDocs.vue'

import IndexProfiles from './views/Profiles/Index.vue'

// Pages
import TheCampus from './views/Pages/TheCampus.vue'
import Administration from './views/Pages/Administration.vue'
import Contacts from './views/Pages/Contacts.vue'

// Admin Pages
import AdminIndexBranches from './views/Admin/Branches/Index.vue'
import AdminCreateBranches from './views/Admin/Branches/Create.vue'
import AdminUpdateBranches from './views/Admin/Branches/Update.vue'

import AdminIndexInformatives from './views/Admin/Informatives/Index.vue'
import AdminCreateInformatives from './views/Admin/Informatives/Create.vue'
import AdminUpdateInformatives from './views/Admin/Informatives/Update.vue'

import AdminIndexLinks from './views/Admin/Links/Index.vue'
import AdminCreateLinks from './views/Admin/Links/Create.vue'
import AdminUpdateLinks from './views/Admin/Links/Update.vue'

import AdminIndexSettings from './views/Admin/Settings/Index.vue'
import AdminCreateSettings from './views/Admin/Settings/Create.vue'
import AdminUpdateSettings from './views/Admin/Settings/Update.vue'

import AdminIndexUsers from './views/Admin/Users/Index.vue'
import AdminUpdateUsers from './views/Admin/Users/Update.vue'


Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { label: 'Home' }
    },
    {
      path: '/acessar',
      name: 'login',
      component: SignIn,
      meta: { label: 'Acessar', icon: 'login' }
    },
    {
      path: '/sair',
      name: 'logout',
      component: SignOut,
      meta: { label: 'SignOut' }
    },
    {
      path: '/admin',
      name: 'dashboard',
      component: Dashboard,
      meta: { label: 'Dashboard' }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
      meta: { label: 'Sobre' }
    },

    // Pages
    { path: '/o-campus/', name: 'the-campus', component: TheCampus, meta: { label: 'O Campus', navbar: true }},
    { path: '/administracao/', name: 'administration', component: Administration, meta: { label: 'Administração', navbar: true }},
    { path: '/contatos/', name: 'contacts', component: Contacts, meta: { label: 'Contatos', navbar: true }},
    // internal modules
    { path: '/ramais/', name: 'index-branches', component: IndexBranches , meta: { menu: 'general', label: 'Ramais' }},
    { path: '/informativos/', name: 'index-informatives', component: IndexInformatives, meta: { menu: 'general', label: 'Informativos', requiresAuth: true }},
    { path: '/frequencias/', name: 'index-frequencies', component: IndexFrequencies, meta: { menu: 'general', label: 'Frequências', requiresAuth: true }},
    { path: '/tickets/', name: 'index-tickets', component: IndexTickets, meta: { menu: 'general', label: 'Abrir chamado' }},
    { path: '/tickets/criar/ti', name: 'create-tickets-ti', component: CreateTicketsTI, meta: { menu: 'general' }},
    { path: '/tickets/criar/infra', name: 'create-tickets-infra', component: CreateTicketsInfra, meta: { menu: 'general' }},
    { path: '/agenda/', name: 'index-agenda', component: IndexAgenda, meta: { menu: 'general', label: 'Agendar consulta', requiresAuth: true }},
    { path: '/agenda/consulta/dentista', name: 'create-agenda-dentist', component: CreateAgendaDentist, meta: { menu: 'general' }},
    { path: '/alunos/', name: 'index-students', component: IndexStudents , meta: { menu: 'general', label: 'Alunos'}},
    { path: '/alunos/consulta/email-academico', name: 'check-academic-email', component: CheckAcademicEmail, meta: { menu: 'general' }},
    { path: '/alunos/solicitacao/diploma', name: 'request-degree', component: RequestDegree, meta: { menu: 'general' }},
    { path: '/alunos/estagio/solicitacao-de-documentos', name: 'internship-docs', component: InternshipDocs, meta: { menu: 'general' }},
    { path: '/alunos/estagio/solicitacao-de-documentos/inicio', name: 'begin-internship-docs', component: BeginInternshipDocs, meta: { menu: 'general' }},
    { path: '/alunos/estagio/solicitacao-de-documentos/durante', name: 'during-internship-docs', component: DuringInternshipDocs, meta: { menu: 'general' }},
    { path: '/alunos/estagio/solicitacao-de-documentos/fim', name: 'ending-internship-docs', component: EndingInternshipDocs, meta: { menu: 'general' }},
    { path: '/perfil/', name: 'index-profiles', component: IndexProfiles, meta: { menu: 'general', label: 'Perfil'}},

    // Admin Pages
    { path: '/admin/ramais/', name: 'admin-index-branches', component: AdminIndexBranches , meta: { admin: true, menu: 'admin', label: 'Ramais' }},
    { path: '/admin/ramais/criar', name: 'admin-create-branches', component: AdminCreateBranches , meta: { admin: true, menu: 'admin', label: 'Criar ramais' }},
    { path: '/admin/ramais/atualizar', name: 'admin-update-branches', component: AdminUpdateBranches , meta: { admin: false, menu: 'admin', label: 'Atualizar ramais' }},

    { path: '/admin/informativos/', name: 'admin-index-informatives', component: AdminIndexInformatives, meta: { admin: true, menu: 'admin', label: 'Informativos' }},
    { path: '/admin/informativos/criar', name: 'admin-create-informatives', component: AdminCreateInformatives, meta: { admin: true, menu: 'admin', label: 'Criar informativo' }},
    { path: '/admin/informativos/atualizar', name: 'admin-update-informatives', component: AdminUpdateInformatives, meta: { admin: false, menu: 'admin', label: 'Atualizar informativos' }},

    { path: '/admin/links/', name: 'admin-index-links', component: AdminIndexLinks, meta: { admin: true, menu: 'admin', label: 'Links' }},
    { path: '/admin/links/criar', name: 'admin-create-links', component: AdminCreateLinks, meta: { admin: true, menu: 'admin', label: 'Criar links' }},
    { path: '/admin/links/atualizar', name: 'admin-update-links', component: AdminUpdateLinks, meta: { admin: true, menu: 'admin', label: 'Atualizar links' }},

    { path: '/admin/configuracoes/', name: 'admin-index-settings', component: AdminIndexSettings, meta: { admin: true, menu: 'admin', label: 'Configurações' }},
    { path: '/admin/configuracoes/criar', name: 'admin-create-settings', component: AdminCreateSettings, meta: { admin: true, menu: 'admin', label: 'Criar configuração' }},
    { path: '/admin/configuracoes/atualizar', name: 'admin-update-settings', component: AdminUpdateSettings, meta: { admin: true, menu: 'admin', label: 'Atualizar configurações' }},

    { path: '/admin/usuarios/', name: 'admin-index-users', component: AdminIndexUsers, meta: { admin: true, menu: 'admin', label: 'Usuários' }},
    { path: '/admin/usuarios/atualizar', name: 'admin-update-users', component: AdminUpdateUsers, meta: { admin: true, menu: 'admin', label: 'Atualizar usuários' }},
  ]

})

router.beforeEach((to, from, next) => {
	if (to.name == 'login') {
		if (window.localStorage.getItem('isAuthenticated') != null) {
			next({ name: 'home' })
		}
	}
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (window.localStorage.getItem('isAuthenticated') == null) {
			next({ name : 'login' })
		} else {
			next()
		}
	}
	next()
})

export default router;
