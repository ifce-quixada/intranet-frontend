import { SnackbarProgrammatic as Snackbar } from 'buefy'

export default {
  showSnackbar: function (message, category) {
    Snackbar.open({
      duration: 10000,
      message: message,
      position: 'is-top',
      type: category
    })
  }
}
