// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  informatives: [],
  informative: {},
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  getInformatives ({commit}) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/informativos/'
    var options = {
      headers: {
        Accept: 'application/json',
        Authorization: 'Bearer ' + window.localStorage.getItem('userToken')
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setInformatives', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  getInformative ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/informativos/' + payload
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setInformative', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  updateInformative ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/informativos/' + payload.id
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('id', payload.id)
		params.append('title', payload.title)
		params.append('link', payload.link)
		params.append('published_at', payload.published_at.toDateString())

    axios.put(url, params, options)
      .then(response => {
        message = 'Informativo atualizado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-informatives' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  createInformative ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/informativos'
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('title', payload.title)
		params.append('link', payload.link)
		params.append('published_at', payload.published_at.toDateString())

    axios.post(url, params, options)
      .then(response => {
        message = 'Informativo criado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-informatives' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  deleteInformative ({commit}, payload) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/admin/informativos/' + payload.id
    var options = {
      headers: {
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

    axios.delete(url, options)
      .then(response => {
        message = 'Informativo removido com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'dashboard' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setInformatives (state, payload) {
    state.informatives = payload
  },
  setInformative (state, payload) {
    state.informative = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
