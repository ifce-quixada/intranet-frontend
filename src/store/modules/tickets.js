// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  sendMail ({commit}, payload) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/tickets/'
    var options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }

  const params = new URLSearchParams()
  params.append('name', payload.name)
  params.append('login', payload.login)
  params.append('sector', payload.sector)
  params.append('email', payload.email)
  params.append('queue', payload.queue)
  params.append('emailto', payload.emailto)
  params.append('subject', payload.subject)
  params.append('content', payload.content)
  params.append('number', payload.number)
  if (payload.queue == 'infra') {
    params.append('chief', payload.chief)
  }

  axios.post(url, params, options)
    .then(response => {
      message = response.data.status_message
      messages.showSnackbar(message, response.data.type_message)
      router.push({path: '/'})
    })
    .catch(error => {
      commit('setErrorMessage', error.toString().split(':')[1])

      var message = '('+ error.response.status + ') '+ error.response.statusText + ' - abrir chamado'
        messages.showSnackbar(message, 'is-danger')
        return error
      })
    .finally(() => {
      commit('setLoading', false)
    })
  },
}

// mutations
const mutations = {
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
