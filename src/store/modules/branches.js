// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  branches: [],
  branch: {},
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  getBranches ({commit}) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/ramais/'
    var options = {
      headers: {
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setBranches', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  getBranch ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/ramais/' + payload
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setBranch', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  updateBranch ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/ramais/' + payload.id
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('id', payload.id)
		params.append('sector', payload.sector)
		params.append('contacts', payload.contacts)
		params.append('branch_line', payload.branch_line)

    axios.put(url, params, options)
      .then(response => {
        message = 'Ramal atualizado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-branches' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  createBranch ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/ramais'
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('sector', payload.sector)
		params.append('contacts', payload.contacts)
		params.append('branch_line', payload.branch_line)

    axios.post(url, params, options)
      .then(response => {
        message = 'Ramal criado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-branches' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  deleteBranch ({commit}, payload) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/admin/ramais/' + payload.id
    var options = {
      headers: {
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

    axios.delete(url, options)
      .then(response => {
        message = 'Ramal removido com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'home' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setBranches (state, payload) {
    state.branches = payload
  },
  setBranch (state, payload) {
    state.branch = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
