// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  step: 0,
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  toBegining ({commit}) {
    commit('setStep', 0)
  },
  toStep ({commit}, payload) {
    commit('setStep', parseInt(payload))
  },
  addStep ({commit}) {
    var currentStep = state.step
    if (currentStep > 3) {
      commit('setStep', currentStep)
    }
    var aux = currentStep + 1
    commit('setStep', aux)
  },
  removeStep ({commit}) {
    var currentStep = state.step
    if (currentStep <= 0) {
      return
    }
    var aux = currentStep - 1
    commit('setStep', aux)
  },
  createEvent ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/agenda/'
    var options = {
      headers: {
        'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded',
				// 'X-Requested-With': 'XMLHttpRequest'
      }
    }

    var message = ''
		const params = new URLSearchParams()
    var eventDate = payload.eventDateTime.toLocaleDateString('pt-br')
    var eventTime = payload.eventDateTime.toLocaleTimeString('pt-br')

		params.append('name', payload.student.name)
		params.append('student_registration', payload.student.student_registration)
		params.append('email_personal', payload.student.email_personal)
		params.append('email_ifce', payload.student.email_ifce)
		params.append('phone_1', payload.student.phone_1)
    params.append('eventDate', eventDate)
    params.append('eventTime', eventTime)

    // params.append('eventTime', payload.envetDateTime.toLocaleTimeString('pt-br'))
		// params.append('startDateTime', payload.eventDateTime.toLocaleString('pt-br'))

    axios.post(url, params, options)
      .then(response => {
        message = 'Agendamento realizado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({path: '/'})
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setStep (state, payload) {
    state.step = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}
