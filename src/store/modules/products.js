// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  products: [],
  product: Object,
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  getProducts ({commit}) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/products/'
    /* var options = {
      withCredentials: true
    }*/
    axios.get(url)
      .then(response => {
        commit('setProducts', response.data)
        return response
      })
      .catch(error => {
        commit('setError', true)
        // commit('setErrorMessage', error.toString().split(':')[1])

        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - index product controller'
        messages.showSnackbar(message, 'warning')
        return error 
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  getProduct ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/products/' + payload.toString() + '/'
    axios.get(url)
      .then(response => {
        console.log(response.data)
        commit('setProduct', response.data)
      })
      .catch(error => {
        console.log('error:', error.toString())
        commit('setError', true)
        commit('setErrorMessage', error.toString().split(':')[1])
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  createProduct ({commit}, payload) {
    commit('setLoading', true)
    axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    var url = Vue.config.urlBase + '/api/products/create/'
    var params = {
      'description': payload.description,
      'price': payload.price
    }
    var data = Object.entries(params)
      .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
      .join('&');

    axios.post(url, data)
    .then(function (response) {
      if (response.status === 200) {
        var message = 'Produto criado com sucesso.'
        messages.showSnackbar(message, 'success')
      }
      router.push({ name: 'index-products' })
      return response
    })
    .catch(function (error) {
      if (error.response.status === 403) {
        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - create controller'
        messages.showSnackbar(message, 'warning')
      } 
    })
    .finally(() => {
      commit('setLoading', false)
    })
  },
  updateProduct ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/products/update/' + payload.id + '/'
      var params = {
      'description': payload.description,
      'price': payload.price
    }
    var data = Object.entries(params)
      .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
      .join('&');

    axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    axios.post(url, data)
    .then(function (response) {
      if (response.status === 200) {
        var message = 'Produto atualizado com sucesso.'
        messages.showSnackbar(message, 'success')
      }
      router.push({ name: 'index-products' })
      return response
    })
    .catch(function (error) {
      if (error.response.status === 403) {
        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - update controller'
        messages.showSnackbar(message, 'warning')
      } 
    })
    .finally(() => {
      commit('setLoading', false)
    })
  },
  deleteProduct ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/products/delete/' + payload.id + '/'

    axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    axios.post(url)
    .then(function (response) {
      if (response.status === 200) {
        var message = 'Produto deletado com sucesso.'
        messages.showSnackbar(message, 'success')
      }
      router.push({ name: 'index-products' })
      return response
    })
    .catch(function (error) {
      if (error.response.status === 403) {
        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - create controller'
        messages.showSnackbar(message, 'warning')
      } 
    })
    .finally(() => {
      commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setProducts (state, payload) {
    state.products = payload
  },
  setProduct (state, payload) {
    state.product = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
