// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  user: null,
  userToken: null,
  isAuthenticated: false,
  error: false,
  loading: false
}

// getters
const getters = {
  isAuthenticated: state => state.isAuthenticated,
  user: state => state.user
}

// actions
const actions = {
  signIn ({commit}, payload) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/login'
    var params = {
      'username': payload.username,
      'password': payload.password
    }
    var data = Object.entries(params)
      .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
      .join('&');

    // axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    axios.post(url, data)
    .then(function (response) {
      if (response.status === 200) {

        window.localStorage.setItem('user', JSON.stringify(response.data.user)) 
        window.localStorage.setItem('isAuthenticated', true) 
        window.localStorage.setItem('userToken', response.data.success.token)
        commit('setUser', response.data.user)
        commit('setUserToken', response.data.success.token)
        commit('setIsAuthenticated', true)

        router.push({path: '/'})
        return response
      }
    })
    .catch(function (error) {
      commit('setError', true)

      if (error.response.status == 500) {
				commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
        message = 'Requisição falhou: erro interno do servidor (500).'
        messages.showSnackbar(message, 'is-danger')
      }

      if (error.response.status === 401) {
				commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
        message = 'Requisição falhou: não autorizado (401).'
        messages.showSnackbar(message, 'is-danger')
        messages.showSnackbar('Verifique usuário e senha informados.', 'is-info')
      } 
      router.push({name: 'login'})
    })
    .finally(() => {
      commit('setLoading', false)
    })
  },
  signOut ({commit}) {
    commit('setLoading', true)
    // var url = Vue.config.urlBase + '/api/logout/'

    // axios.defaults.headers.post['X-CSRFToken'] = payload.csrftoken 
    // axios.post(url)
    // .then(function (response) {
    //  if (response.status === 200) {
    //    var message = response.data.msg
    //    messages.showSnackbar(message, 'success')
    //  }
    //  window.localStorage.setItem('user', null) 
    //  window.localStorage.setItem('isAuthenticated', false) 
    //  window.localStorage.setItem('userToken', null)
    //  commit('setUser', null)
    //  commit('setUserToken', null)
    //  commit('setIsAuthenticated', false)

    //  router.push({ path : '/' })
    //  return response
    //})
    //.catch(function (error) {
    //  console.log(error)
    //  if (error.response.status === 403) {
    //    var message = '('+ error.response.status + ') '+ error.response.statusText + ' - create controller'
    //    messages.showSnackbar(message, 'warning')
    //  } 
    //})
    //.finally(() => {
    //  commit('setLoading', false)
    //})

    window.localStorage.removeItem('user') 
    window.localStorage.removeItem('isAuthenticated') 
    window.localStorage.removeItem('userToken')
    commit('setUser', null)
    commit('setUserToken', null)
    commit('setIsAuthenticated', false)

    commit('setLoading', false)
    messages.showSnackbar('Usuário deslogado com sucesso.', 'is-info')
    router.push({ name: 'home' })
  }
}

// mutations
const mutations = {
  setUser (state, payload) {
    state.user = payload
  },
  setUserToken (state, payload) {
    state.userToken = payload
  },
  setIsAuthenticated (state, payload) {
    state.isAuthenticated = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
