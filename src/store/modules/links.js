// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  links: [],
  link: {},
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  getLinks ({commit}) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/links/'
    var options = {
      headers: {
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setLinks', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  getLink ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/links/' + payload
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setLink', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  updateLink ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/links/' + payload.id
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('id', payload.id)
		params.append('link', payload.link)
		params.append('description', payload.description)

    axios.put(url, params, options)
      .then(response => {
        message = 'Link atualizado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-links' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  createLink ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/links'
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('link', payload.link)
		params.append('description', payload.description)

    axios.post(url, params, options)
      .then(response => {
        message = 'Link criado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-links' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  deleteLink ({commit}, payload) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/admin/links/' + payload.id
    var options = {
      headers: {
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

    axios.delete(url, options)
      .then(response => {
        message = 'Link removido com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'dashboard' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setLinks (state, payload) {
    state.links = payload
  },
  setLink (state, payload) {
    state.link = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
