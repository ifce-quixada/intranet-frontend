// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  error: false,
  loading: false,
  student: {},
}

// getters
const getters = {}

// actions
const actions = {
  sendRequestDegreeMail ({commit}, payload) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/estudantes/tickets/diploma'
    var options = {
      timeout: 10,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    }

    let params = new FormData()

    params.append('studentName', payload.name)
    params.append('course', payload.course)
    params.append('registration', payload.registration)
    params.append('email', payload.email)
    params.append('emailto', payload.emailto)
    params.append('phone', payload.phone)
    params.append('request', payload.request)
    params.append('queue', 'cca')

    for (var i = 0; i < payload.files.length; i++) {
      params.append('files[' + i + ']', payload.files[i])
    }

    axios.post(url, params, options)
      .then(response => {
        if (response.data.status_code === '400') {
          message = response.data.status_message
          messages.showSnackbar(message, response.data.type_message)
          return false
        }
        message = response.data.status_message
        messages.showSnackbar(message, response.data.type_message)
        router.push({path: '/'})
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])

        var message = '('+ error.response.status + ') '+ error.response.statusText + ' - abrir chamado'
        messages.showSnackbar(message, 'is-danger')
        return error
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  searchStudent ({commit}, payload) {
    var message = ''
    var url = Vue.config.urlBase + '/api/estudantes/' + payload.registration
    var options = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    }
    if (payload.registration == '' || payload.cpf == '') {
      message = 'Ambos os campos são obrigatórios.'
      messages.showSnackbar(message, 'is-danger')
      return 1
    }
    commit('setLoading', true)
    axios.get(url, options)
      .then((response) => {
        if (response.data.cpf == payload.cpf) {
          commit('setStudent', response.data)
        } else {
          this.dataError = true
        }
        return response
      })
      .catch(error => {
        if (error.response.status == 500) {
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          this.error = true
          this.dataError = false
          message = 'Matrícula não encontrada. Preencha manualmente o formulário.'
          messages.showSnackbar(message, 'is-danger')
        } else {
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
          messages.showSnackbar(message, 'is-danger')
        }
      })
      .finally(() => {
        commit('setLoading', false)
        this.isLoading = false
      })
  },
  printInternshipDocs ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/estudantes/estagio/documentos/'
    var options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      responseType: 'blob',
    }

    var dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric' }
    var date0 = payload.internship.dates[0].toLocaleDateString('pt-BR', dateOptions)
    var date1 = payload.internship.dates[1].toLocaleDateString('pt-BR', dateOptions)
    var time0 = payload.internship.startTime.toLocaleTimeString('pt-BR')
    var time1 = payload.internship.endTime.toLocaleTimeString('pt-BR')
    var timeDiff = payload.internship.endTime - payload.internship.startTime

    var msec = timeDiff;
    var hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    var mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;

    const params = new URLSearchParams()
    params.append('studentName', payload.student.name)
    params.append('studentRegistration', payload.student.student_registration)
    params.append('studentEmail', payload.student.email_ifce)
    params.append('studentPhone', payload.student.phone_1)
    params.append('studentBirthday', payload.student.birthday)
    params.append('studentRG', payload.student.rg)
    params.append('studentCPF', payload.student.cpf)
    params.append('studentAddress', payload.student.address)
    params.append('studentAddressStreetNumber', payload.student.street_number)
    if (payload.student.address_suplement == undefined || payload.student.address_suplement == "") {
      params.append('studentAddressSuplement', '-')
    } else {
      params.append('studentAddressSuplement', payload.student.address_suplement)
    }
    params.append('studentNeighborhood', payload.student.address_neighborhood)
    params.append('studentPostalCode', payload.student.postal_code)
    params.append('studentCity', payload.student.city)
    params.append('studentState', payload.student.state)
    params.append('studentCourse', payload.student.course)
    params.append('studentCourseType', payload.student.course_type)
    params.append('studentSemester', payload.student.period)
    params.append('internshipShift', payload.internship.shift)
    params.append('internshipType', payload.internship.internshipType)
    params.append('internshipPeriod', date0 + ' - ' + date1)
    params.append('internshipTime', time0 + ' - ' + time1)
    params.append('internshipMode', payload.internship.mode)
    params.append('internshipTimeDuration', hh * 5)
    params.append('internshipActivities', payload.internship.activities)
    params.append('internshipResults', payload.internship.results)
    params.append('internshipExecution', payload.internship.execution)
    params.append('internshipEnsurance', payload.internship.ensurance)
    params.append('internshipEnsuranceCompany', payload.internship.ensurance_company)
    params.append('internshipCompensation', payload.internship.compensation)
    params.append('internshipCompensationValue', payload.internship.compensation_value)
    params.append('internshipCompensationWrittenValue', payload.internship.compensation_written_value)
    params.append('internshipTransportAssistance', payload.internship.transport_assistance)
    params.append('internshipTransportAssistanceValue', payload.internship.transport_assistance_value)
    params.append('internshipTransportAssistanceWrittenValue', payload.internship.transport_assistance_written_value)

    // company fields
    params.append('companyName', payload.internship.company_name)
    params.append('companyRepresentative', payload.internship.company_representative)
    params.append('companyField', payload.internship.company_field)
    params.append('companyCNPJ', payload.internship.company_cnpj)
    params.append('companyPhone', payload.internship.company_phone)
    params.append('companyEmail', payload.internship.company_email)
    params.append('companyAddress', payload.internship.company_address)
    params.append('companyAddressStreetNumber', payload.internship.company_street_number)
    if (payload.internship.company_address_suplement == undefined || payload.internship.company_address_suplement == "") {
      params.append('companyAddressSuplement', '-')
    } else {
      params.append('companyAddressSuplement', payload.internship.company_address_suplement)
    }
    params.append('companyNeighborhood', payload.internship.company_address_neighborhood)
    params.append('companyPostalCode', payload.internship.company_postal_code)
    params.append('companyCity', payload.internship.company_city)
    params.append('companyState', payload.internship.company_state)
    params.append('companySupervisorName', payload.internship.company_supervisor_name)
    params.append('companySupervisorPhone', payload.internship.company_supervisor_phone)
    params.append('companySupervisorPosition', payload.internship.company_supervisor_position)

    // advisor fields
    params.append('advisorName', payload.internship.advisor_name)
    params.append('advisorEmail', payload.internship.advisor_email)
    params.append('advisorPhone', payload.internship.advisor_phone)

    // other fields
    params.append('stage', payload.stage)

    axios.post(url, params, options)
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url;
        var pdf_filename = payload.student.student_registration+ '_documentos.pdf'
        link.setAttribute('download', pdf_filename)
        document.body.appendChild(link)
        link.click()
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])

        var message = '('+ error.response.status + ') '+ error.response.statusText + ' '
        messages.showSnackbar(message, 'is-danger')
        return error
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  printDuringInternshipDocs ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/estudantes/estagio/documentos/'
    var options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      responseType: 'blob',
    }

    const params = new URLSearchParams()
    // common fields
    // students
    params.append('studentName', payload.student.name)
    params.append('studentCourse', payload.student.course)
    params.append('studentRegistration', payload.student.student_registration)

    // company
    params.append('companyName', payload.internship.company_name)

    // ------ //
    // stage 'relatorio-mensal'  fields
    if (payload.stage == 'relatorio-mensal') {
      // students
      params.append('studentPhone', payload.student.phone_1)

      // company
      params.append('companySupervisorName', payload.internship.company_supervisor_name)

      // internship
      var dateOptions = { year: 'numeric', month: 'numeric' }
      var date0 = payload.internship.date_report.toLocaleDateString('pt-BR', dateOptions)
      params.append('advisorName', payload.internship.advisor_name)
      params.append('internshipReportDate', date0)
      params.append('internshipHoursPerDay', payload.internship.hours_per_day)
      params.append('internshipDaysPerMonth', payload.internship.days_per_month)
      params.append('internshipHoursPerMonth', payload.internship.hours_per_month)
      params.append('internshipTotalHours', payload.internship.total_hours)
      params.append('internshipActivities', payload.internship.activities)
    }

    // ------ //
    // stage 'termo-aditivo' fields
    //
    if (payload.stage == 'termo-aditivo') {
      // contract
      //
      params.append('contractExtension', payload.contract.extension)
      params.append('contractWorkHour', payload.contract.workhour)
      params.append('contractCompensation', payload.contract.compensation)
      params.append('contractSupervisor', payload.contract.supervisor)
      params.append('contractActivities', payload.contract.activities)
      params.append('contractOther', payload.contract.other)

      // testing if user wants to change his period
      if (payload.contract.extension == 1) {
        let dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric' }
        let date0 = payload.contract.period[0].toLocaleDateString('pt-BR', dateOptions)
        let date1 = payload.contract.period[1].toLocaleDateString('pt-BR', dateOptions)
        params.append('contractPeriod', date0 + ' - ' + date1)
      }

      // testing if user wants to change his work hour
      if (payload.contract.workhour == 1) {
        if (payload.contract.group == 1 || payload.contract.group == 2 || payload.contract.group == 3) {
          var contract_start_time1 = payload.contract.startTime1.toLocaleTimeString('pt-BR')
          var contract_end_time1 = payload.contract.endTime1.toLocaleTimeString('pt-BR')
          params.append('contractHoursGroup1', contract_start_time1 + " - " + contract_end_time1)
          params.append('contractWeekdays1', payload.contract.weekdays1)
        }
        if (payload.contract.group == 2 || payload.contract.group == 3) {
          var contract_start_time2 = payload.contract.startTime2.toLocaleTimeString('pt-BR')
          var contract_end_time2 = payload.contract.endTime2.toLocaleTimeString('pt-BR')
          params.append('contractHoursGroup2', contract_start_time2 + " - " + contract_end_time2)
          params.append('contractWeekdays2', payload.contract.weekdays2)
        }
        if (payload.contract.group == 3) {
          var contract_start_time3 = payload.contract.startTime3.toLocaleTimeString('pt-BR')
          var contract_end_time3 = payload.contract.endTime3.toLocaleTimeString('pt-BR')
          params.append('contractHoursGroup3', contract_start_time3 + " - " + contract_end_time3)
          params.append('contractWeekdays3', payload.contract.weekdays3)
        }
      }

      // testing if user wants to change his compensation
      if (payload.contract.compensation == 1) {
        dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric' }
        date0 = payload.contract.compensation_new_date.toLocaleDateString('pt-BR', dateOptions)
        params.append('contractCompensationNewDate', date0)
        params.append('contractCompensationValue', payload.contract.compensation_value)
        params.append('contractCompensationWrittenValue', payload.contract.compensation_written_value)
      }

      // testing if user wants to change his supervisor
      if (payload.contract.supervisor == 1) {
        params.append('contractNewSupervisor', payload.contract.new_supervisor)
        params.append('contractNewSupervisorOccupation', payload.contract.new_supervisor_occupation)
      }

      // testing if user wants to change his current activities
      if (payload.contract.activities == 1) {
        params.append('contractNewActivities', payload.contract.new_activities)
      }

      // testing if user wants to change other terms
      if (payload.contract.other == 1) {
        params.append('contractNewTerms', payload.contract.new_terms)
      }

    }

    // other fields
    params.append('stage', payload.stage)

    axios.post(url, params, options)
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url;
        var pdf_filename = payload.student.student_registration+ '_documentos.pdf'
        link.setAttribute('download', pdf_filename)
        document.body.appendChild(link)
        link.click()
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])

        var message = '('+ error.response.status + ') '+ error.response.statusText + ' '
        messages.showSnackbar(message, 'is-danger')
        return error
      })
      .finally(() => {
        commit('setLoading', false)
      })
  },
  printEndingInternshipDocs ({commit}, payload) {
    commit('setLoading', true)
    var url = Vue.config.urlBase + '/api/estudantes/estagio/documentos/'
    var options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      responseType: 'blob',
    }

    var dateOptions = { year: 'numeric', month: 'numeric', day: 'numeric' }
    var date0 = payload.internship.dates[0].toLocaleDateString('pt-BR', dateOptions)
    var date1 = payload.internship.dates[1].toLocaleDateString('pt-BR', dateOptions)

    const params = new URLSearchParams()
    // common fields
    // students
    params.append('studentName', payload.student.name)
    params.append('studentCourse', payload.student.course)
    params.append('studentRegistration', payload.student.student_registration)

    // company
    params.append('companyName', payload.internship.company_name)
    params.append('companySupervisorName', payload.internship.company_supervisor_name)

    // internship
    params.append('internshipPeriod', date0 + ' - ' + date1)
    params.append('internshipTotalHours', payload.internship.total_hours)

    // ------ //
    // stage 'fim'  fields
    if (payload.stage == 'fim') {
      // students
      params.append('studentRegistration', payload.student.student_registration)
      params.append('studentCourseType', payload.student.course_type)
      params.append('studentSituation', payload.student.situation)
      params.append('studentOtherSituation', payload.student.other_situation)
      params.append('studentJobSituation', payload.student.job_situation)

      // internship
      params.append('internshipSector', payload.internship.sector)
      params.append('internshipPlacement', payload.internship.placement)
      params.append('internshipCompanyActivities', payload.internship.company_activities)
      params.append('internshipDevelopedActivities', payload.internship.developed_activities)
      params.append('internshipDificulties', payload.internship.dificulties)
      params.append('internshipCompanyQualities', payload.internship.company_qualities)
      params.append('internshipAcquiredSkills', payload.internship.acquired_skills)
      params.append('internshipCompanyAssistance', payload.internship.company_assistance)
      params.append('internshipInstitutionAssistance', payload.internship.institution_assistance)
      params.append('internshipSelfAvaliationAsIntern', payload.internship.self_avaliation_as_intern)
      params.append('internshipSelfAvaliationAsEmployee', payload.internship.self_avaliation_as_employee)
      params.append('internshipSubjectSuggestion', payload.internship.subject_suggestion)
      params.append('internshipGeneralSuggestion', payload.internship.general_suggestion)
    }

    // ------ //
    // stage 'relatorio-final' fields
    // campos relacionados a aluno do curso de graduacao
    if (payload.stage == 'relatorio-final') {
      params.append('internshipSector', payload.internship.sector)
      params.append('internshipCompanyActivities', payload.internship.company_activities)
      params.append('internshipDificulties', payload.internship.dificulties)
      params.append('internshipCompare', payload.internship.compare)

      params.append('scoreGeneralKnowledge', payload.internship.score_general_knowledge)
      params.append('scoreInitative', payload.internship.score_initiative)
      params.append('scoreCriativity', payload.internship.score_criativity)
      params.append('scoreEthics', payload.internship.score_ethics)
      params.append('scoreOrganization', payload.internship.score_organization)
      params.append('scoreSocial', payload.internship.score_social)
      params.append('scoreCooperation', payload.internship.score_cooperation)
      params.append('scoreLeadership', payload.internship.score_leadership)
      params.append('scorePunctuality', payload.internship.punctuality)
      params.append('scoreTakesResponsibility', payload.internship.takes_responsibility)
      params.append('scoreIntegration', payload.internship.score_integration)
      params.append('scoreEngagement', payload.internship.score_engagement)
      params.append('scoreManagement', payload.internship.score_management)

      params.append('internshipFinalConsiderations', payload.internship.final_considerations)
    }


    // ------ //
    // stage 'fim-empresa' fields
    //
    if (payload.stage == 'fim-empresa') {
      // students
      params.append('studentEmail', payload.student.email_ifce)
      params.append('studentPhone', payload.student.phone_1)
      params.append('studentBirthday', payload.student.birthday)
      params.append('studentRG', payload.student.rg)
      params.append('studentCPF', payload.student.cpf)
      params.append('studentAddress', payload.student.address)
      params.append('studentAddressStreetNumber', payload.student.street_number)
      if (payload.student.address_suplement == undefined || payload.student.address_suplement == "") {
        params.append('studentAddressSuplement', '')
      } else {
        params.append('studentAddressSuplement', payload.student.address_suplement)
      }

      // company fields
      params.append('companyPhone', payload.internship.company_phone)
      params.append('companyCNPJ', payload.internship.company_cnpj)
      params.append('companyField', payload.internship.company_field)
      params.append('companyAddress', payload.internship.company_address)
      params.append('companyAddressStreetNumber', payload.internship.company_street_number)
        if (payload.internship.company_address_suplement == undefined || payload.internship.company_address_suplement == "") {
          params.append('companyAddressSuplement', '')
        } else {
        params.append('companyAddressSuplement', payload.internship.company_address_suplement)
        }
      params.append('companyNeighborhood', payload.internship.company_address_neighborhood)
      params.append('companyPostalCode', payload.internship.company_postal_code)
      params.append('companyCity', payload.internship.company_city)
      params.append('companyState', payload.internship.company_state)
      params.append('companySupervisorPhone', payload.internship.company_supervisor_phone)

      // internship
      params.append('activitiesPerformedByIntern', payload.internship.developed_activities)
      params.append('scoreLearning', payload.internship.score_learning)
      params.append('scoreSecurity', payload.internship.score_security)
      params.append('scoreInterest', payload.internship.score_interest)
      params.append('scoreProactivity', payload.internship.score_proactivity)
      params.append('scorePracticalKnowledge', payload.internship.score_practical_knowledge)
      params.append('scoreProductivity', payload.internship.score_productivity)
      params.append('scoreDiscipline', payload.internship.score_discipline)
      params.append('scoreInterpersonalRelationship', payload.internship.interpersonal_relationship)
      params.append('scoreTakesResponsability', payload.internship.takes_responsability)
      params.append('scorePunctuality', payload.internship.punctuality)
      params.append('scoreAttendance', payload.internship.attendance)

      params.append('avaliationMethods', payload.internship.avaliation_methods)
      params.append('generalSuggestions', payload.internship.general_suggestions)
    }

    // other fields
    params.append('stage', payload.stage)

    axios.post(url, params, options)
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url;
        var pdf_filename = payload.student.student_registration+ '_documentos.pdf'
        link.setAttribute('download', pdf_filename)
        document.body.appendChild(link)
        link.click()
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])

        var message = '('+ error.response.status + ') '+ error.response.statusText + ' '
        messages.showSnackbar(message, 'is-danger')
        return error
      })
      .finally(() => {
        commit('setLoading', false)
      })
  }
}

// mutations
const mutations = {
  setStudent (state, payload) {
    state.student = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
