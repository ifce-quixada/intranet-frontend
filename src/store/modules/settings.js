// import things here
import Vue from 'vue'
import axios from 'axios'
import router from '@/router'

import messages from '@/utils/messages'

const state = {
  settings: [],
  setting: {},
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  gs ({commit}) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/admin/configuracoes'
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setSettings', response.data)
        return response
      })
    .catch(error => {
      commit('setError', true)
      commit('setErrorMessage', error.toString().split(':')[1])
      message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
			messages.showSnackbar(message, 'is-danger')
    })
    .finally(() => {
      commit('setLoading', false)
    })
  },
  getSettings ({commit}) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/configuracoes/'
    var options = {
      headers: {
        Accept: 'application/json',
      }
    }
    axios.get(url, options)
      .then(response => {
        if (Object.keys(response.data).length === 0 && response.data.constructor === Object) {
          commit('setSettings', false)
        } else {
          commit('setSettings', response.data)
        }
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  getSetting ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/configuracoes/' + payload
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setSetting', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },

  createSetting ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/configuracoes'
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('current_semester', payload.current_semester)
		params.append('first_school_day', payload.first_school_day)
		params.append('last_school_day', payload.last_school_day)

    axios.post(url, params, options)
      .then(response => {
        message = 'Configuração criada com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-settings' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  updateSetting ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/admin/configuracoes/' + payload.id
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('id', payload.id)
		params.append('current_semester', payload.current_semester)
		params.append('first_school_day', payload.first_school_day)
		params.append('last_school_day', payload.last_school_day)

    axios.put(url, params, options)
      .then(response => {
        message = 'Configuração atualizada com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'admin-index-settings' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  deleteSetting ({commit}, payload) {
    commit('setLoading', true)
    var message = ''
    var url = Vue.config.urlBase + '/api/admin/configuracoes/' + payload.id
    var options = {
      headers: {
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

    axios.delete(url, options)
      .then(response => {
        message = 'Configuração removida com sucesso.'
        messages.showSnackbar(message, 'is-success')
        router.push({ name: 'dashboard' })
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setSettings (state, payload) {
    state.settings = payload
  },
  setSetting (state, payload) {
    state.setting = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
