// import things here
import Vue from 'vue'
import axios from 'axios'

import messages from '@/utils/messages'

const state = {
  profiles: [],
  profile: {},
  error: false,
  loading: false
}

// getters
const getters = {}

// actions
const actions = {
  getProfile ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/perfil/' + payload.id
    var options = {
      headers: {
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }
    axios.get(url, options)
      .then(response => {
        commit('setProfile', response.data)
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  },
  updateProfile ({commit}, payload) {
    commit('setLoading', true)
		var message = ''
    var url = Vue.config.urlBase + '/api/perfil/' + payload.id
    var options = {
      headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('userToken'),
        Accept: 'application/json'
      }
    }

		const params = new URLSearchParams()
		params.append('id', payload.id)
		params.append('work_schedule', payload.work_schedule)
		params.append('work_load', payload.work_load)

    axios.put(url, params, options)
      .then(response => {
        message = 'Usuário atualizado com sucesso.'
        messages.showSnackbar(message, 'is-success')
        return response
      })
      .catch(error => {
        commit('setErrorMessage', error.toString().split(':')[1])
        commit('setError', true)

        if (error.response.status == 500) {
          commit('setErrorMessage', 'Requisição falhou: erro interno do servidor (500).')
          message = 'Requisição falhou: erro interno do servidor (500).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 401) {
          commit('setErrorMessage', 'Requisição falhou: não autorizado (401).')
          message = 'Requisição falhou: não autorizado (401).'
          messages.showSnackbar(message, 'is-danger')
        } else if (error.response.status === 404) {
          commit('setErrorMessage', 'Requisição falhou: recurso não encontrado (404).')
          message = 'Requisição falhou: recurso não encontrado (404).'
          messages.showSnackbar(message, 'is-danger')
				} else {
					commit('setErrorMessage', error.toString().split(':')[1])
          message = 'Requisição falhou: ' + error.toString().split(':')[1] + '.'
					messages.showSnackbar(message, 'is-danger')
        }

        return error
      })
      .finally(() => {
        commit('setLoading', false)
    })
  }
}

// mutations
const mutations = {
  setProfile (state, payload) {
    state.profile = payload
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setError (state, payload) {
    state.error = payload
  },
  setErrorMessage (state, payload) {
    state.errorMessage = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
