import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// local modules
import auth from './modules/auth'
import agenda from './modules/agenda'
import branches from './modules/branches'
import links from './modules/links'
import frequencies from './modules/frequencies'
import informatives from './modules/informatives'
import students from './modules/students'
import tickets from './modules/tickets'
import settings from './modules/settings'
import users from './modules/users'
import profiles from './modules/profiles'

export default new Vuex.Store({
  modules: {
    agenda,
    branches,
    links,
    frequencies,
    informatives,
    students,
    tickets,
    settings,
    users,
    profiles,
    auth
  },
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
